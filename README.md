# epicsarchiver-config

This repository is used to store all PVs to archive.
This is in **alpha stage** and only to evaluate the concept.


## Introduction

For each IOC, the list of PVs to archive should be saved in a file named `<ioc name>.archive`.
The file should be put under a directory named after the archiver appliance fully-qualified domain name.

Here is the list of current archiver appliances:

- **archiver-01.tn.esss.lu.se** for the FEB
- **archiver-02.tn.esss.lu.se** for Cryo, ODH and TS2
- **ics-archiver11.tn.esss.lu.se** for testing
- **nas-beam-01.cslab.esss.lu.se** for Beam Diagnostic test

archiver-01 and archiver-02 are part of the same cluster. So all PVs can be accessed from both appliances.
But PVs are still allocated to a specific server (they don't have the same EPICS_CA_ADDR_LIST).

## Archive File format

The files shall be in CSV format (space separated) and include one PV name per line.
A file can also include the name of the policy to force (optional).
Empty lines and lines starting with "#" (comments) are allowed.

Here is an example:

```
# PV name    Policy
ISrc-010:PwrC-CoilPS-01:CurS
ISrc-010:PwrC-CoilPS-01:CurR    1Hz
# Comments are allowed
LEBT-010:Vac-VCG-30000:PrsStatR
```

The string after the PV name should be an existing policy to force.
In the above example, the policy "1Hz" would be applied to the PV "ISrc-010:PwrC-CoilPS-01:CurR".
The default policy would be applied to other PVs.

## Workflow

When pushing to master, the PVs are automatically added to the archiver.

The `process_archives.py` script looks at files that changed since last commit.
All the PVs from those files are sent to the proper appliance for archiving.

PV deletion is currently not handled.
