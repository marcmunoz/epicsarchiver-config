#!/usr/bin/env python
# Copyright (c) 2018 European Spallation Source ERIC
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import click
import shlex
import subprocess
from pathlib import Path
from pprint import pprint
from epicsarchiver import ArchiverAppliance


def run(cmd, cwd=None):
    """Run the shell command and return the result"""
    args = shlex.split(cmd)
    result = subprocess.run(args, check=True, stdout=subprocess.PIPE, cwd=cwd)
    return result.stdout.decode("utf-8").strip()


def get_files_modified(commit_before_sha, commit_sha):
    """Return the files modified between the 2 commits"""
    result = run(
        f"git log --name-only --pretty=format: {commit_before_sha}..{commit_sha}"
    )
    return [Path(filename) for filename in set(result.split("\n")) if filename]


def get_files_per_archiver(archives):
    """Return the archive files per archiver"""
    p = Path(".")
    archivers = [
        x for x in p.iterdir() if x.is_dir() and x.name not in (".git", "policies")
    ]
    result = {}
    for archiver in archivers:
        result[archiver.name] = [
            filepath for filepath in archives if archiver in filepath.parents
        ]
    return result


@click.command()
@click.option("--before-sha", help="Previous commit sha", required=True)
@click.option("--sha", help="Current commit sha [default: HEAD]", default="HEAD")
@click.option("--verbose", "-v", is_flag=True, help="Increase verbosity")
def cli(before_sha, sha, verbose):
    """Send PVs to the archiver(s) for files changed between 2 commits"""
    archive_files = [
        filepath
        for filepath in get_files_modified(before_sha, sha)
        if filepath.suffix == ".archive"
    ]
    archivers = get_files_per_archiver(archive_files)
    if verbose:
        pprint(archivers)
    for hostname, files in archivers.items():
        if files:
            click.echo(
                f"Sending PVs from {','.join([str(f) for f in files])} to {hostname}"
            )
            archiver = ArchiverAppliance(hostname)
            result = archiver.archive_pvs_from_files(files, appliance=archiver.identity)
            if verbose:
                pprint(result)
        else:
            click.echo(f"No new PV for {hostname}")


if __name__ == "__main__":
    cli()
